<?php
/**
 * @package Student Management Plugin
 */

/*
Plugin Name: Student Management Plugin
Description: WordPress Student Management Plugin
Version: 1.0.0
Author: Emrah Sifoğlu
Author URI: http://www.emrahsifoglu.com
License: A "Slug" license name e.g. GPL2
*/
/*  Copyright 2015  Emrah Sifoğlu  (email : emrahsifoglu@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
if (!function_exists('add_action')) {
	echo '403';
	exit;
}

/**********
 * defines
 **********/

define('SMP_VERSION', '1.0.0' );
define('SMP', 'sm');
define('SMP_PLUGIN_URL', plugin_dir_url(__FILE__ ));
define('SMP_PLUGIN_DIR', plugin_dir_path(__FILE__ ));
define('SMP_SOURCE', 'source/');
define('SMP_TEMPLATES', 'templates/');
define('SMP_STYLES', SMP_PLUGIN_URL.'css/');
define('SMP_SCRIPTS', SMP_PLUGIN_URL.'js/');
define('SMP_IMAGES', SMP_PLUGIN_URL.'images/');

require_once SMP_SOURCE.'class.db.php';
require_once SMP_SOURCE.'abstract.plugin.php';
require_once SMP_SOURCE.'abstract.model.php';
require_once SMP_SOURCE.'class.student.php';
require_once SMP_SOURCE.'class.exam.php';

if(!class_exists('WP_Student_Management_Plugin')) {
	class WP_Student_Management_Plugin extends Abstract_SMP {

		/**
		 * Construct the plugin object
		 *
		 * @return \WP_Student_Management_Plugin
		 */
		protected function init() {
			$this->add_action('admin_init', 'admin_init');
			$this->add_action('admin_menu', 'add_page');
			// students
			$this->add_action('admin_enqueue_scripts', 'sm_students_scripts');
			$this->add_action('wp_ajax_create_student', 'sm_create_student_callback');
			$this->add_action('wp_ajax_update_student', 'sm_update_student_callback');
			$this->add_action('wp_ajax_delete_student', 'sm_delete_student_callback');
			// exams
			$this->add_action('admin_enqueue_scripts', 'sm_exams_scripts');
			$this->add_action('wp_ajax_create_exam', 'sm_create_exam_callback');
			$this->add_action('wp_ajax_update_exam', 'sm_update_exam_callback');
			$this->add_action('wp_ajax_delete_exam', 'sm_delete_exam_callback');
			//
			$this->add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'sm_settings');
		}

		/**
		 * Activate the plugin
		 *
		 * @return void
		 */
		public static function activate() {
			if (!current_user_can('activate_plugins')) return;
			$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
			check_admin_referer( "activate-plugin_{$plugin}" );
			SM_DB::create_table('students', "id mediumint(9) NOT NULL AUTO_INCREMENT,
						name varchar(55) NOT NULL,
						date_of_birth DATE NOT NULL,
						PRIMARY KEY (id),
						KEY idx_name (name)");
			SM_DB::create_table('exams', "id mediumint(9) NOT NULL AUTO_INCREMENT,
						exam_name varchar(55) NOT NULL,
						student_id mediumint(9) NOT NULL,
						mark tinyint(1) NOT NULL,
						date_of_exam DATE NOT NULL,
						PRIMARY KEY id (id)");
			SM_DB::query('ALTER TABLE wp_exams
						ADD CONSTRAINT FK_exams
						FOREIGN KEY (student_id) REFERENCES wp_students(id)
						ON UPDATE CASCADE
						ON DELETE CASCADE');
			add_option('Activated_Plugin', 'SM-Plugin-Slug');
		}

		/**
		 * Deactivate the plugin
		 *
		 * @return void
		 */
		public static function deactivate() {
			if (!current_user_can('activate_plugins')) return;
			$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
			check_admin_referer( "deactivate-plugin_{$plugin}" );
			$options = get_option( 'sm_settings' );
			if ($options['delete_database']) self::delete_table();
			if (!$options['keep_settings']) delete_option('sm_settings');
		}

		/**
		 * Uninstall the plugin
		 *
		 * @return void
		 */
		public static function uninstall() {
			if (!current_user_can( 'activate_plugins' )) return;
			check_admin_referer( 'bulk-plugins' );

			// Important: Check if the file is the one
			// that was registered during the uninstall hook.
			//if ( __FILE__ != WP_UNINSTALL_PLUGIN) return; ?
			delete_option('sm_settings');
			self::delete_table();
		}

		public function admin_init(){
			$this->init_settings();
		}

		public function init_settings(){
			register_setting('sm_settings_group', 'sm_settings');
			add_settings_section(
				'sm_settings_sections_id', // ID
				'Custom Settings', // Title
				array( $this, 'custom_settings_callback' ), // Callback
				'sm_settings_sections' // Page
			);
			add_settings_field(
				'delete_database', // ID
				'Delete database', // Title
				array( $this, 'delete_database_callback' ), // Callback
				'sm_settings_sections', // Page
				'sm_settings_sections_id' // Section
			);
			add_settings_field(
				'keep_settings', // ID
				'Keep settings saved', // Title
				array( $this, 'delete_settings_callback' ), // Callback
				'sm_settings_sections', // Page
				'sm_settings_sections_id' // Section
			);
		}

		/**
		 * Print the custom settings text
		 */
		public function custom_settings_callback() {
			print 'When plugin is deactivated perform actions below.';
		}

		function delete_database_callback() {
			$options = get_option( 'sm_settings' );
			$html = '<input type="checkbox" id="cb_dd" name="sm_settings[delete_database]" value="1"'.checked(1, $options['delete_database'], false).'/>';
			echo $html;
		}

		function delete_settings_callback(){
			$options = get_option( 'sm_settings' );
			$html = '<input type="checkbox" id="cb_ks" name="sm_settings[keep_settings]" value="1"'.checked(1, $options['keep_settings'], false).'/>';
			echo $html;
		}

		/**
		 * Add page(s)
		 *
		 * @return void
		 */
		public function add_page() {
			add_options_page('Settings', 'Student Management', 'manage_options', 'sm-settings', array($this, 'sm_settings_page'));
			add_menu_page(__('Student Management'), __('Student Management'), 'manage_options', 'sm_mainmenu');
			add_submenu_page('sm_mainmenu', __('Manage Students'), __('Manage students'), 'manage_options', 'sm-students', array($this, 'sm_students_render'));
			add_submenu_page('sm_mainmenu', __('Manage Exams'), __('Manage exams'), 'manage_options', 'sm-exams', array($this, 'sm_exams_render'));
			$this->remove_submenu('sm_mainmenu', 'manage_options');
		}

		/**
		 * @param string $links
		 * @return string
		 */
		public function sm_settings($links) {
			$settings_link = '<a href="options-general.php?page=sm-settings">Settings</a>';
			array_unshift($links, $settings_link);
			return $links;
		}

		/**
		 * Setting menu callback
		 *
		 * @return void
		 */
		public function sm_settings_page() {
			if(!current_user_can('manage_options')) wp_die(__('Permission check.'));
			ob_start();
			require_once SMP_TEMPLATES.'admin_settings_tpl.php';
			echo ob_get_clean();
		}

		/**
		 * Manage students callback
		 *
		 * @return void
		 */
		public function sm_students_render(){
			if(!current_user_can('manage_options')) wp_die(__('Permission check.'));
			$students = new Student();
			extract(array('students' => $students->fetch(), 'token' => wp_create_nonce('sm_perform_crud_nonce')));
			ob_start();
			require_once SMP_TEMPLATES.'admin_student.tpl.php';
			echo ob_get_clean();
		}

		public function sm_students_scripts($hook) {
			if($hook != 'student-management_page_sm-students') return;
			$this->add_styles(array(
				array(
					'handle' => 'sm-admin-style-1',
					'src' => '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css'
				),
				array(
					'handle' => 'sm-admin-style-2',
					'src' => SMP_STYLES.'grid.css'
				),
				array(
					'handle' => 'sm-admin-style-4',
					'src' => SMP_STYLES.'model-contain.css'
				),
				array(
					'handle' => 'sm-admin-style-5',
					'src' => SMP_STYLES.'students.css'
				)
			));
			$this->add_scripts(array(
				array(
					'handle' => 'sm-admin-script-1',
					'src' => '//code.jquery.com/ui/1.11.2/jquery-ui.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-2',
					'src' => SMP_SCRIPTS.'helper.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-3',
					'src' => SMP_SCRIPTS.'student.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-4',
					'src' => SMP_SCRIPTS.'students.js',
					'version' => ''
				)
			));
		}

		public function sm_create_student_callback() {
			$this->check_permission();
			if (filter_input(INPUT_POST, '_name') && filter_input(INPUT_POST, '_birthday')){
				$name = $_POST['_name'];
				$birthday = $_POST['_birthday'];
				if( filter_var($name, FILTER_VALIDATE_REGEXP,
						array("options"=>array("regexp"=>'/^(?=[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z]{2})[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z0-9 ._-]{2,50}$/'))) &&
				    filter_var($birthday, FILTER_VALIDATE_REGEXP,
					    array("options"=>array("regexp"=>'/^([0-9]{4})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{2})$/')))){
					$student = new Student();
					$student->id = 0;
					echo $student->save(array(
						'name' => $name,
						'date_of_birth' => $birthday
					));
				}
			}
			die();
		}

		public function sm_update_student_callback() {
			if (filter_input(INPUT_POST, '_id') && filter_input(INPUT_POST, '_name') && filter_input(INPUT_POST, '_birthday')){
				$id = $_POST['_id'];
				$name = $_POST['_name'];
				$birthday = $_POST['_birthday'];
				if( filter_var($id, FILTER_VALIDATE_INT) &&
					filter_var($name, FILTER_VALIDATE_REGEXP,
						array("options"=>array("regexp"=>'/^(?=[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z]{2})[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z0-9 ._-]{2,50}$/'))) &&
				    filter_var($birthday, FILTER_VALIDATE_REGEXP,
					    array("options"=>array("regexp"=>'/^([0-9]{4})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{2})$/')))){
					$student = new Student();
					$student->id = $id;
					echo $student->save(array(
						'name' => $name,
						'date_of_birth' => $birthday
					));
				}
			}
			die();
		}

		public function sm_delete_student_callback() {
			$this->check_permission();
			if (filter_input(INPUT_POST, '_id')){
				$id = $_POST['_id'];
				if( filter_var($id, FILTER_VALIDATE_INT)){
					$student = new Student();
					$student->id = $id;
					echo $student->delete();
				}
			}
			die();
		}

		/**
		 * Manage exams callback
		 *
		 * @return void
		 */
		public function sm_exams_render(){
			if(!current_user_can('manage_options'))	wp_die(__('Permission check.'));
			$students = new Student();
			$exam = new Exam();
			$exams = $exam->load();
			$exams_for_students = [];
			foreach($exams as $e) {
				$exams_for_students[$e->student_id][] = $e;
			}
			extract(array('students' => $students->fetch(), 'exams_for_students' => $exams_for_students ,'token' => wp_create_nonce('sm_perform_crud_nonce')));
			ob_start();
			require_once SMP_TEMPLATES.'admin_exam.tpl.php';
			echo ob_get_clean();
		}

		public function sm_exams_scripts($hook) {
			if($hook != 'student-management_page_sm-exams') return;
			$this->add_styles(array(
				array(
					'handle' => 'sm-admin-style-1',
					'src' => '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css'
				),
				array(
					'handle' => 'sm-admin-style-2',
					'src' => SMP_STYLES.'grid.css'
				),
				array(
					'handle' => 'sm-admin-style-4',
					'src' => SMP_STYLES.'model-contain.css'
				),
				array(
					'handle' => 'sm-admin-style-5',
					'src' => SMP_STYLES.'exams.css'
				)
			));
			$this->add_scripts(array(
				array(
					'handle' => 'sm-admin-script-1',
					'src' => '//code.jquery.com/ui/1.11.2/jquery-ui.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-2',
					'src' => SMP_SCRIPTS.'helper.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-3',
					'src' => SMP_SCRIPTS.'exam.js',
					'version' => ''
				),
				array(
					'handle' => 'sm-admin-script-4',
					'src' => SMP_SCRIPTS.'exams.js',
					'version' => ''
				)
			));
		}

		public function sm_create_exam_callback() {
			$this->check_permission();
			if (filter_input(INPUT_POST, '_student_id') && filter_input(INPUT_POST, '_mark') && filter_input(INPUT_POST, '_name') && filter_input(INPUT_POST, '_date')){
				$name = $_POST['_name'];
				$student_id = $_POST['_student_id'];
				$mark = $_POST['_mark'];
				$date = $_POST['_date'];
				if( filter_var($mark, FILTER_VALIDATE_INT) &&
					filter_var($student_id, FILTER_VALIDATE_INT) &&
				    filter_var($name, FILTER_VALIDATE_REGEXP,
						array("options" => array("regexp"=>'/^(?=[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z]{2})[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z0-9 ._-]{2,50}$/'))) &&
				    filter_var($date, FILTER_VALIDATE_REGEXP,
					    array("options" => array("regexp"=>'/^([0-9]{4})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{2})$/')))){
					$exam = new Exam();
					$exam->id = 0;
					echo $exam->save(array(
						'exam_name' => $name,
						'student_id' => $student_id,
						'mark' => $mark,
						'date_of_exam' => $date
					));
				}
			}
			die();
		}

		public function sm_update_exam_callback() {
			$this->check_permission();
			if (filter_input(INPUT_POST, '_id') && filter_input(INPUT_POST, '_mark') && filter_input(INPUT_POST, '_name') && filter_input(INPUT_POST, '_date')){
				$id = $_POST['_id'];
				$name = $_POST['_name'];
				$mark = $_POST['_mark'];
				$date = $_POST['_date'];
				if( filter_var($mark, FILTER_VALIDATE_INT) &&
				    filter_var($id, FILTER_VALIDATE_INT) &&
				    filter_var($name, FILTER_VALIDATE_REGEXP,
					    array("options" => array("regexp"=>'/^(?=[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z]{2})[ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄäA-Za-z0-9 ._-]{2,50}$/'))) &&
				    filter_var($date, FILTER_VALIDATE_REGEXP,
					    array("options" => array("regexp"=>'/^([0-9]{4})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{2})$/')))){
					$exam = new Exam();
					$exam->id = $id;
					echo $exam->save(array(
						'exam_name' => $name,
						'mark' => $mark,
						'date_of_exam' => $date
					));
				}
			}
			die();
		}

		public function sm_delete_exam_callback() {
			$this->check_permission();
			if (filter_input(INPUT_POST, '_id')){
				$id = $_POST['_id'];
				if( filter_var($id, FILTER_VALIDATE_INT)){
					$exam = new Exam();
					$exam->id = $id;
					echo $exam->delete();
				}
			}
			die();
		}

		private function check_permission(){
			if (!$this->isAJAX() || !$this->isRequestMethod('POST') || !check_ajax_referer('sm_perform_crud_nonce', '_token', false))
				die('Permission check.');
		}

		private static function delete_table(){
			SM_DB::query('ALTER TABLE wp_exams DROP FOREIGN KEY FK_exams');
			SM_DB::drop_table('exams');
			SM_DB::drop_table('students');
		}
	}
}

if(class_exists('WP_Student_Management_Plugin')) {
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('WP_Student_Management_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('WP_Student_Management_Plugin', 'deactivate'));
	register_uninstall_hook(__FILE__, array('WP_Student_Management_Plugin', 'uninstall'));

	function load_plugin() {
		if(is_admin() && get_option('Activated_Plugin') == 'SM-Plugin-Slug') {
			delete_option('Activated_Plugin');
			/* do some stuff once right after activation */
		}
	}

	add_action('admin_init','load_plugin');
	add_shortcode('sm_students', function(){
		$students = new Student();
		extract(array('students' => $students->fetch()));
		ob_start();
		include SMP_TEMPLATES.'students_table.tpl.php';
		return ob_get_clean();
	});
	add_shortcode('sm_exams', function($atts){
		$atts = shortcode_atts(array(
			'id' => 0,
		), $atts );
		$exam = new Exam();
		extract(array('exams' => $exam->findByStudentId($atts['id'])));
		ob_start();
		include SMP_TEMPLATES.'exam_table.tpl.php';
		return ob_get_clean();
	});
	// instantiate the plugin class
	if (is_admin()) $wp_plugin_management_template = new WP_Student_Management_Plugin();
}