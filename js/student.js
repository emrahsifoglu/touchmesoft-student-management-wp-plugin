function Student () {
    this.id = 0;
}

Student.prototype.controller = "";
Student.prototype.token = "";
Student.prototype.id = 0;
Student.prototype.name = "";
Student.prototype.birthday = "";
Student.prototype.errors = [];
Student.prototype.action = '';

Student.prototype.setToken = function(token) {
    this.token = token;
};

Student.prototype.setController = function(controller) {
    this.controller = controller;
};

Student.prototype.setId = function(id) {
    if (isGreaterThanZero(id)) this.id = id;
};

Student.prototype.setName = function(name) {
    if (isNameValid(name, 2, 50)) this.name = name;
};

Student.prototype.setBirthday = function(birthday) {
    if (isDate(birthday)) this.birthday = birthday;
};

Student.prototype.getController = function() {
    return this.controller;
};

Student.prototype.getToken = function() {
    return this.token;
};

Student.prototype.getAction = function() {
    return this.action;
};

Student.prototype.getId = function() {
    return this.id;
};

Student.prototype.getName = function() {
    return this.name;
};

Student.prototype.getBirthday = function() {
    return this.birthday;
};

Student.prototype.prepare = function(){
    this.id = 0;
    this.clear();
};

Student.prototype.clear = function(){
    this.name = "";
    this.birthday = "";
};

Student.prototype.getErrors = function() {
    this.errors = [];
    if (this.token == "") this.errors.push({name:"token", msg:"Token is not valid."});
    if (this.controller == "") this.errors.push({name:"controller", msg:"Controller is not valid."});
    if (this.name == "") this.errors.push({name:"name", msg:"Name is not valid."});
    if (this.birthday == "") this.errors.push({name:"birthday", msg:"Birthday is not valid."});
    return this.errors;
};

Student.prototype.isValid = function() {
    return (this.getErrors().length == 0);
};

Student.prototype.save = function(onSuccess, onError, onComplete){
    this.action = 'create_student';
    var url = this.getController();
    var data = {
        _id: this.getId(),
        _name:this.getName(),
        _birthday:this.getBirthday(),
        _token:this.getToken()
    };
    if (this.getId() > 0) this.action = 'update_student';
    data['action'] = this.action;
    callService(url, data, onSuccess, onError, onComplete);
};

Student.prototype.destroy = function(onSuccess, onError, onComplete){
    this.action = 'delete_student';
    var data = { action:this.action,  _id:this.getId(), _token:this.getToken() };
    callService(this.getController(), data, onSuccess, onError, onComplete);
};

function callService(url, data, onSuccess, onError, onComplete){
    jQuery.ajax({
        type: 'POST',
        url: url,
        data : data,
        success: function (data, textStatus, jqXHR) {
            onSuccess(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            onError(jqXHR, textStatus, errorThrown);
        },
        complete: function(){
            onComplete();
        }
    });
}