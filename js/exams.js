jQuery(document).ready(function($) {
    var token = $('#token').val();
    var name = $('#name');
    var mark = $('#mark');
    var date = $('#date');
    var students = $('#students');
    var exams_for_students = $('#exams-for-students');
    var temp_efs_html = $("#exams-for-student-0").html();
    var tips = $( ".validateTips" );
    var exam = new Exam();
    var confirm = $("#dialog-confirm");
    var passed = $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 435,
        width: 350,
        modal: true,
        buttons: {
            Save: onSave,
            Cancel: onCancel
        },
        open: function(){
            tips.html('All form fields are required.');
        },
        close: onClose

    });

    var form = passed.find("form").on("submit", function( event ) {
        event.preventDefault();
        onSave();
    });

    $("#create-exam").click(function() {
        students.prop('disabled', false);
        passed.dialog({
            title:'Create new exam'
        });
        passed.dialog("open");
    });

    date.datepicker({
        startDate: '1950-01-01',
        endDate: new Date().getFullYear().toString()+'-12-31',
        dateFormat: 'yy-mm-dd'
    }).val('');

    function onSave(){
        exam.clear();
        exam.setDate(date.val());
        exam.setName(name.val());
        exam.setMark(mark.val());
        exam.setStudentId(students.val());
        if (exam.isValid()){
            exam.save(onSuccess, onError, onComplete);
        } else {
            tips.html('');
            $.each(exam.getErrors(), function(i, e){
                tips.append('<b>'+e.msg+'</b><br/>');
            });
        }
    }

    function onCancel(){
        passed.dialog("close");
    }

    function onClose(){
        form[0].reset();
    }

    function onSuccess(data, textStatus, jqXHR){
        var current_action = exam.getAction();
        switch (current_action){
            case 'create_exam':
                if (isGreaterThanZero(data)){
                    if ($('#exams-for-student-'+exam.getStudentId()).length != 0){
                        var exams_html = $(exams_for_students).find('.exams').html().
                            split('[[exam_id]]').join(data).
                            replace('[[exam_name]]', exam.getName()).
                            replace('[[date]]', exam.getDate()).
                            replace('[[mark]]', exam.getMark());
                        $('#exams-for-student-'+exam.getStudentId()).find('.exams').append(exams_html);
                    } else {
                        var efs_html = temp_efs_html.
                            split('[[exam_id]]').join(data).
                            replace('[[student_name]]', students.find("option:selected").text()).
                            replace('[[exam_name]]', exam.getName()).
                            replace('[[date]]', exam.getDate()).
                            replace('[[mark]]', exam.getMark());
                        exams_for_students.append('<div class="exams-for-student" id="exams-for-student-'+exam.getStudentId()+'">'+efs_html+'</div>');
                    }
                    form[0].reset();
                }
                break;
            case 'update_exam':
                var current_exam_tr = $('#exam-'+exam.getId()).closest('tr');
                current_exam_tr.find('td:eq(0)').html(exam.getName());
                current_exam_tr.find('td:eq(1)').html(exam.getDate());
                current_exam_tr.find('td:eq(2)').html(exam.getMark());
                exam.prepare();
                passed.dialog('close');
                break;
            case 'delete_exam':
                $("#exam-"+exam.getId()).closest('tr').remove();
                unbindExamControls(exam.getId());
                exam.prepare();
                confirm.dialog('close');
                break;
        }
    }

    function onError(jqXHR, textStatus, errorThrown){
        console.log(textStatus);
        console.log(errorThrown);
    }

    function onComplete(){
        console.log('complete');
    }

    function deleteExam(event){
        event.preventDefault();
        var id = $(this).attr('href');
        confirm.dialog({
            resizable: false,
            height:165,
            width:600,
            modal: true,
            buttons: {
                Delete: function() {
                    exam.setId(id);
                    exam.destroy(onSuccess, onError, onComplete);
                },
                Cancel: function() {
                    $(this).dialog("close");
                }
            }
        });
    }

    function updateExam(event){
        event.preventDefault();
        var id = $(this).attr('href');
        var current_student_id = $(this).closest('.exams-for-student').attr('id').replace('exams-for-student-', '');
        var current_exam_tr = $(this).closest('tr');
        students.val(current_student_id);
        exam.setId(id);
        name.val(current_exam_tr.find('td:eq(0)').html());
        date.val(current_exam_tr.find('td:eq(1)').html());
        mark.val(current_exam_tr.find('td:eq(2)').html());
        students.prop('disabled', true);
        passed.dialog({
            title:'Update existing exam'
        });
        passed.dialog("open");
    }

    function bindExamsControls(){
        $(".delete-exam").live("click", deleteExam);
        $(".update-exam").live("click", updateExam);
    }

    function unbindExamControls(id){
        $(".delete-exam", "#exam-"+id).unbind("click", deleteExam);
        $(".update-exam", "#exam-"+id).unbind("click", updateExam);
    }

    form[0].reset();
    exam.prepare();
    exam.setToken(token);
    exam.setController(ajaxurl);
    bindExamsControls();
});