jQuery(document).ready(function($) {

    var token = $('#token').val();
    var name = $('#name');
    var birth_date = $('#birth-date');
    var temp_student_html = $("#student-0").html();
    var student = new Student();
    var tips = $(".validateTips");
    var confirm = $("#dialog-confirm");
    var enroll = $("#dialog-form").dialog({
            autoOpen: false,
            height: 320,
            width: 350,
            modal: true,
            buttons: {
                Save: onSave,
                Cancel: onCancel
            },
            open: function(){
                tips.html('All form fields are required.');
            },
            close: onClose

        });

    var form = enroll.find("form").on("submit", function( event ) {
        event.preventDefault();
        onSave();
    });

    $("#create-student").click(function() {
        student.prepare();
        enroll.dialog({
            title:'Create new student'
        });
        enroll.dialog("open");
    });

    birth_date.datepicker({
        startDate: '1950-01-01',
        endDate: new Date().getFullYear().toString()+'-12-31',
        dateFormat: 'yy-mm-dd'
    }).val('');

    function onSave(){
        student.clear();
        student.setName(name.val());
        student.setBirthday(birth_date.val());
        if (student.isValid()){
            student.save(onSuccess, onError, onComplete);
        } else {
            tips.html('');
            $.each(student.getErrors(), function(i, e){
                tips.append('<b>'+e.msg+'</b><br/>');
            });
        }
    }

    function onCancel(){
        enroll.dialog( "close" );
    }

    function onClose(){
        form[0].reset();
    }

    function onSuccess(data, textStatus, jqXHR){
        var current_action = student.getAction();
        switch (current_action){
            case 'create_student':
                if (isGreaterThanZero(data)){
                    appendStudent(data, student.getName(), student.getBirthday());
                    student.prepare();
                    form[0].reset();
                }
                break;
            case 'update_student':
                $('#student-'+student.getId()).find('td:eq(0)').html(student.getName());
                $('#student-'+student.getId()).find('td:eq(1)').html(student.getBirthday());
                student.prepare();
                enroll.dialog('close');
                break;
            case 'delete_student':
                $("#student-"+student.getId()).remove();
                unbindStudentControl(student.getId());
                student.prepare();
                confirm.dialog('close');
                break;
        }
    }

    function onError(jqXHR, textStatus, errorThrown){
        console.log(textStatus);
        console.log(errorThrown);
    }

    function onComplete(){
        console.log('complete');
    }

    function deleteStudent(event){
        event.preventDefault();
        var id = $(this).attr('href');
        confirm.dialog({
            resizable: false,
            height:165,
            width:600,
            modal: true,
            buttons: {
                Delete: function() {
                    student.setId(id);
                    student.destroy(onSuccess, onError, onComplete);
                },
                Cancel: function() {
                    $(this).dialog("close");
                }
            }
        });
    }

    function updateStudent(event){
        event.preventDefault();
        var id = $(this).attr('href');
        student.setId(id);
        name.val($(this).closest('tr').find('td:eq(0)').html());
        birth_date.val($(this).closest('tr').find('td:eq(1)').html());
        enroll.dialog({
            title:'Update existing student'
        });
        enroll.dialog("open");
    }

    function appendStudent(id, name, birth_date){
        var student_td = temp_student_html.split('[[id]]').join(id).replace('[[name]]', name).replace('[[birth_date]]', birth_date);
        $('#students').append('<tr class="student" id="student-'+id+'">'+student_td+'</tr>');
    }

    function bindStudentControls(){
        $(".delete-student").live("click", deleteStudent);
        $(".update-student").live("click", updateStudent);
    }

    function unbindStudentControl(id){
        $(".delete-student", "#student-"+id).unbind("click", deleteStudent);
        $(".update-student", "#student-"+id).unbind("click", updateStudent);
    }

    form[0].reset();
    student.prepare();
    student.setController(ajaxurl);
    student.setToken(token);
    bindStudentControls();
});