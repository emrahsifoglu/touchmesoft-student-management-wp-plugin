var patterns = {
    name     : "(?=[charsA-Za-z]{min})[charsA-Za-z0-9 ._-]{min,max}",
    date     : /^([0-9]{4})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{2})$/
};

var chars = 'ĞÜŞİÖÇğüşiıöçŠšZzŽžÕõÄä';

function createNamePattern(pattern_str, chars, min, max){
    var mapObj = { chars:chars, min:min, max:max };
    return pattern_str.replace(/chars|min|max/gi, function(matched) {
        return mapObj[matched];
    });
}

function isNameValid(name, min, max) {
    return new RegExp(createNamePattern('^'+patterns.name+'$', chars, min, max)).test(name);
}

function isDate(date){
    return patterns.date.test(date);
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isGreaterThanZero(n) {
    return isNumber(n) && n > 0;
}