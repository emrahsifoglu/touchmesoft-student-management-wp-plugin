function Exam () {
    this.id = 0;
}

Exam.prototype.controller = "";
Exam.prototype.token = "";
Exam.prototype.id = 0;
Exam.prototype.student_id = 0;
Exam.prototype.mark = 0;
Exam.prototype.name = "";
Exam.prototype.date = "";
Exam.prototype.errors = [];
Exam.prototype.action = "";

Exam.prototype.setToken = function(token) {
    this.token = token;
};

Exam.prototype.setController = function(controller) {
    this.controller = controller;
};

Exam.prototype.setId = function(id) {
    if (isGreaterThanZero(id)) this.id = id;
};

Exam.prototype.setStudentId = function(student_id) {
    if (isGreaterThanZero(student_id)) this.student_id = student_id;
};

Exam.prototype.setMark = function(mark) {
    if (isGreaterThanZero(mark)) this.mark = mark;
};

Exam.prototype.setName = function(name) {
    if (isNameValid(name, 2, 50)) this.name = name;
};

Exam.prototype.setDate = function(date) {
    if (isDate(date)) this.date = date;
};

Exam.prototype.getController = function() {
    return this.controller;
};

Exam.prototype.getToken = function() {
    return this.token;
};

Exam.prototype.getAction = function() {
    return this.action;
};

Exam.prototype.getId = function() {
    return this.id;
};

Exam.prototype.getStudentId = function() {
    return this.student_id;
};

Exam.prototype.getMark = function() {
    return this.mark;
};

Exam.prototype.getName = function() {
    return this.name;
};

Exam.prototype.getDate = function() {
    return this.date;
};

Exam.prototype.prepare = function(){
    this.id = 0;
    this.clear();
};

Exam.prototype.clear = function(){
    this.student_id = 0;
    this.mark = 0;
    this.name = "";
    this.date = "";
};

Exam.prototype.getErrors = function() {
    this.errors = [];
    if (this.token == "") this.errors.push({name:"token", msg:"Token is not valid."});
    if (this.controller == "") this.errors.push({name:"controller", msg:"Controller is not valid."});
    if (this.student_id == 0) this.errors.push({name:"student_id", msg:"Student(id) is not valid."});
    if (this.name == "") this.errors.push({name:"name", msg:"Name is not valid."});
    if (this.mark == 0) this.errors.push({name:"mark", msg:"Mark is not valid."});
    if (this.date == "") this.errors.push({name:"date", msg:"Date is not valid."});
    return this.errors;
};

Exam.prototype.isValid = function() {
    return (this.getErrors().length == 0);
};

Exam.prototype.save = function(onSuccess, onError, onComplete){
    this.action = 'create_exam';
    var url = this.getController();
    var data = {
        _id: this.getId(),
        _student_id:this.getStudentId(),
        _mark:this.getMark(),
        _name:this.getName(),
        _date:this.getDate(),
        _token:this.getToken()
    };
    if (this.getId() > 0) this.action = 'update_exam';
    data['action'] = this.action;
    callService(url, data, onSuccess, onError, onComplete);
};

Exam.prototype.destroy = function(onSuccess, onError, onComplete){
    this.action = 'delete_exam';
    var data = { action:this.action,  _id:this.getId(), _token:this.getToken() };
    callService(this.getController(), data, onSuccess, onError, onComplete);
};

function callService(url, data, onSuccess, onError, onComplete){
    jQuery.ajax({
        type: 'POST',
        url: url,
        data : data,
        success: function (data, textStatus, jqXHR) {
            onSuccess(data, textStatus, jqXHR);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            onError(jqXHR, textStatus, errorThrown);
        },
        complete: function(){
            onComplete();
        }
    });
}