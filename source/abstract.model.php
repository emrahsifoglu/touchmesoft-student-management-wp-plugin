<?php
if(!class_exists('Abstract_SMP_Model')) {
	abstract class Abstract_SMP_Model {

		/**
		 * @var SM_DB
		 */
		public $db = null;
		public $id = 0;
		public $table_name = '';

		public function __construct($table_name) {
			$this->table_name = $table_name;
			$this->db = SM_DB::get_instance();
		}

		public function save($fields_values){
			if ($this->id == 0){
				return $this->create($fields_values);
			} else {
				return $this->update($fields_values);
			}
		}

		public function fetch(){
			return $this->db->select($this->table_name);
		}

		public function create($fields_values){
			return $this->db->insert($this->table_name, $fields_values);
		}

		public function read(){

		}

		public function update($fields_values){
			return $this->db->update($this->table_name, $fields_values, array('id' => $this->id));
		}

		public function delete(){
			return $this->db->delete($this->table_name, array('id' => $this->id));
		}

	}
}