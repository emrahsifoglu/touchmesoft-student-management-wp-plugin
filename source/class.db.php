<?php
if (!class_exists('SM_DB')) :
	class SM_DB {

		/**
		 * Don't leave it open to injections:
		 */
		private static $instance = false;
		public static function get_instance() {
			if ( ! self::$instance )
				self::$instance = new self();
			return self::$instance;
		}

		private function __construct() { }

		/**
		 * @param string $table_name
		 * @param string $fields
		 * @return void
		 */
		public static function create_table($table_name, $fields){
			global $wpdb;
			if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") !== $table_name) {
				$charset_collate = $wpdb->get_charset_collate();
				$sql = 'CREATE TABLE '.$wpdb->prefix.$table_name." ($fields) $charset_collate;";
				require_once(ABSPATH. 'wp-admin/includes/upgrade.php');
				dbDelta($sql);
			}
		}

		/**
		 * @param string $table_name
		 * @return void
		 */
		public static function drop_table($table_name){
			global $wpdb;
			$wpdb->query('DROP TABLE IF EXISTS '.$wpdb->prefix.$table_name);
		}

		/**
		 * @param string $sql
		 * @return void
		 */
		public static function query($sql){
			global $wpdb;
			$wpdb->query($sql);
		}

		/**
		 * @param string $table_name
		 * @param array $field_values
		 * @return int
		 */
		public static function insert($table_name, $field_values = array()){
			global $wpdb;
			$wpdb->insert(
				$wpdb->prefix.$table_name, $field_values
			);
			return $wpdb->insert_id;
		}

		/**
		 * @param string $table_name
		 * @return mixed
		 */
		public static function select($table_name){
			global $wpdb;
			return $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.$table_name);
		}

		/**
		 * @param $table_name
		 * @param array $field_values
		 * @param array $where
		 * @return int
		 */
		public static function update($table_name, $field_values = array(), $where = array()){
			global $wpdb;
			$wpdb->update($wpdb->prefix.$table_name, $field_values, $where);
			return $wpdb->rows_affected;
		}

		/**
		 * @param string $table_name
		 * @param array $where
		 * @return int
		 */
		public static function delete($table_name, $where = array()){
			global $wpdb;
			$wpdb->delete($wpdb->prefix.$table_name, $where);
			return $wpdb->rows_affected;
		}

		/**
		 * @param string $sql
		 * @return mixed
		 */
		public static function get_results($sql){
			global $wpdb;
			return $wpdb->get_results($sql);
		}
	}
endif;