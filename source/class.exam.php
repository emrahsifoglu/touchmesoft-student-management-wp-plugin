<?php
if(!class_exists('Exam')) {
	class Exam extends Abstract_SMP_Model {

		public function __construct(){
			parent::__construct('exams');
		}

		public function load(){
			return $this->db->get_results('Select wp_exams.id as exam_id, wp_exams.exam_name, wp_exams.mark, wp_exams.date_of_exam, wp_students.id as student_id, wp_students.name as student_name from wp_exams JOIN wp_students on wp_students.id = wp_exams.student_id');
		}

		public function findByStudentId($id){
			return $this->db->get_results('Select wp_exams.id as exam_id, wp_exams.exam_name, wp_exams.mark, wp_exams.date_of_exam, wp_students.id as student_id, wp_students.name as student_name from wp_exams JOIN wp_students on wp_students.id = wp_exams.student_id where wp_students.id = '.$id);
		}
	}
}