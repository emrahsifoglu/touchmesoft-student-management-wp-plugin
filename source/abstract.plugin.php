<?php
if(!class_exists('Abstract_SMP')) {
	abstract class Abstract_SMP {

		public function __construct( ) {
			$this->init();
		}

		abstract protected function init();

		/**
		 * Load components/3rd-party libraries
		 *
		 * @return void
		 */
		protected static function load() {

		}

		/**
		 * @param $action
		 * @param string $function
		 * @param int $priority
		 * @param int $accepted_args
		 * @return void
		 */
		protected function add_action( $action, $function = '', $priority = 10, $accepted_args = 1 ) {
			add_action( $action, array($this, $function == '' ? $action : $function ), $priority, $accepted_args );
		}

		/**
		 * @param $filter
		 * @param $function
		 * @param int $priority
		 * @param int $accepted_args
		 * @return void
		 */
		protected function add_filter( $filter, $function, $priority = 10, $accepted_args = 1 ) {
			add_filter( $filter, array($this, $function == '' ? $filter : $function ), $priority, $accepted_args );
		}

		/**
		 * @param array $scripts
		 * @return void
		 */
		protected function add_scripts($scripts = array()){
			if (is_array($scripts) && !empty($scripts)) {
				foreach ($scripts as $script) {
					wp_register_script($script['handle'], $script['src'], array('jquery'), $script['version'], false);
					wp_enqueue_script($script['handle']);
				}
			}
		}

		/**
		 * @param array $styles
		 * @return void
		 */
		protected function add_styles($styles = array()){
			if (is_array($styles) && !empty($styles)) {
				foreach ($styles as $style) {
					wp_register_style($style['handle'] , $style['src'] );
					wp_enqueue_style($style['handle']);
				}
			}
		}

		/**
		 * @param string $methodType;
		 * @return bool
		 */
		protected function isRequestMethod($methodType){
			return ($this->getRequestMethod() === $methodType) ? true : false;
		}

		/**
		 * @return string
		 */
		protected function getRequestMethod(){
			return strtoupper($_SERVER['REQUEST_METHOD']);
		}

		/**
		 * @return bool
		 */
		protected function isAJAX() {
			return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
		}

		/**
		 * @param $menu_name
		 * @param $submenu_name
		 * @return void
		 */
		protected function remove_submenu($menu_name, $submenu_name) {
			global $submenu;
			$menu = $submenu[$menu_name];
			if (!is_array($menu)) return;
			foreach ($menu as $submenu_key => $submenu_object) {
				if (in_array($submenu_name, $submenu_object)) {// remove menu object
					unset($submenu[$menu_name][$submenu_key]);
					return;
				}
			}
		}
	}
}
