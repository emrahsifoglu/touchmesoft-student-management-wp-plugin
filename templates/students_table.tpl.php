<div>
	<h2>Students list</h2>
	<div>
		<table id="students">
			<tr>
				<th>Student name</th>
				<th>Birth date</th>
			</tr>
			<?php
			foreach( $students as $student) {
				echo '<tr class="student" id="student-'.$student->id.'">
							<td>'.$student->name.'</td>
							<td>'.$student->date_of_birth.'</td>
						</tr>';
			}
			?>
		</table>
	</div>
</div>