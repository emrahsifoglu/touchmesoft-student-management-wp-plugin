<div class="wrap">
	<div id="icon-options-general" class="icon32"></div>
	<h2>Students list</h2>
	<div id="poststuff">
		<div id="dialog-confirm" title="Delete student" style="display: none">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These record will be permanently deleted and cannot be recovered. Are you sure?</p>
		</div>
		<div id="dialog-form" title="create/update">
			<p class="validateTips">All form fields are required.</p>
			<form>
				<fieldset>
					<label for="name">Name</label>
					<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all">
					<label for="birth-date">Birth date</label>
					<input type="text" name="birth-date" id="birth-date" class="text ui-widget-content ui-corner-all">
					<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
				</fieldset>
			</form>
		</div>
		<button id="create-student">Create a new student</button>
		<h1></h1>
		<div class="ui-widget">
			<table id="students" class="grid ui-widget ui-widget-content">
				<tr class="ui-widget-header">
					<th>Student name</th>
					<th>Birth date</th>
					<th></th>
				</tr>
				<tr id="student-0" style="display: none">
					<td>[[name]]</td>
					<td>[[birth_date]]</td>
					<td>
						<div style="text-align: right;">
							<a class="delete-student crud" id="student-[[id]]" href="[[id]]">
								<img class="crud" src="<?=SMP_IMAGES?>/delete.png">
							</a>
							<a class="update-student crud" id="student-[[id]]" href="[[id]]">
								<img class="crud" src="<?=SMP_IMAGES?>/update.png">
							</a>
						</div>
					</td>
				</tr>
				<?php
				foreach( $students as $student) {
					echo '<tr class="student" id="student-'.$student->id.'">
								<td>'.$student->name.'</td>
								<td>'.$student->date_of_birth.'</td>
								<td>
									<div style="text-align: right;">
										<a class="delete-student crud" id="student-'.$student->id.'" href="'.$student->id.'">
											<img class="crud" src="'.SMP_IMAGES.'delete.png">
										</a>
										<a class="update-student crud" id="student-'.$student->id.'" href="'.$student->id.'">
											<img class="crud" src="'.SMP_IMAGES.'update.png">
										</a>
									</div>
								</td>
							 </tr>';
				}
				?>
			</table>
		</div>
		<?php if (is_admin()) printf('<input type="hidden" name="token" id="token" value="%s"/>', $token); ?>
		<br class="clear">
	</div> <!-- #poststuff -->
</div> <!-- .wrap -->