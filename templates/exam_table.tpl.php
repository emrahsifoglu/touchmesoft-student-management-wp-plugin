<div>
	<h2>Exams for student: <?=$exams[0]->student_name?></h2>
	<table class="grid ui-widget ui-widget-content">
		<thead>
		<tr>
			<th>Name</th>
			<th>Date</th>
			<th>Mark</th>
		</tr>
		</thead>
		<tbody class="exams">
		<?php
		foreach($exams as $exam){
			?>
			<tr>
				<td><?=$exam->exam_name?></td>
				<td><?=$exam->date_of_exam?></td>
				<td><?=$exam->mark?></td>
			</tr>
		<?php
		}
		?>
		</tbody>
	</table>
</div>