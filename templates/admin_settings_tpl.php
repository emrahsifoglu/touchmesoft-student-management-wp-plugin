<div class="wrap">
	<h2>Student Management Plugin Settings</h2>
	<form method="post" action="options.php">
		<?php settings_fields('sm_settings_group'); ?>
		<?php do_settings_sections('sm_settings_sections'); ?>
		<?php submit_button(); ?>
	</form>
</div>