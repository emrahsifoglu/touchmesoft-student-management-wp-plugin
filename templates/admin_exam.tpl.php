<div class="wrap">
	<div id="icon-options-general" class="icon32"></div>
	<h2>Exams list</h2>
	<div id="poststuff">
		<div id="dialog-confirm" title="Delete student" style="display: none">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These record will be permanently deleted and cannot be recovered. Are you sure?</p>
		</div>
		<div id="dialog-form" title="create/update">
			<p class="validateTips">All form fields are required.</p>
			<form>
				<fieldset>
					<label for="students">Student
						<select id="students" name="students">
							<option value="-1"></option>
							<?php
							foreach( $students as $student) {
								echo '<option value="'.$student->id.'">'.$student->name.'</option>';
							}
							?>
						</select>
					</label>
					<label for="name">Name</label>
					<input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all">
					<label for="date">Date</label>
					<input type="text" name="date" id="date" class="text ui-widget-content ui-corner-all">
					<label for="mark">Mark
					<input type="number" name="mark" id="mark" value="0"></label>
					<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
				</fieldset>
			</form>
		</div>
		<button id="create-exam">Create a new exam for an existing student</button>
		<h1></h1>
		<div id="exams-for-students" class="ui-widget">
			<div class="exams-for-student" id="exams-for-student-0" style="display: none;">
				<h2>Exams for student: [[student_name]]</h2>
				<table class="grid ui-widget ui-widget-content">
					<thead>
						<tr>
							<th>Name</th>
							<th>Date</th>
							<th>Mark</th>
							<th></th>
						</tr>
					</thead>
					<tbody class="exams">
						<tr>
							<td>[[exam_name]]</td>
							<td>[[date]]</td>
							<td>[[mark]]</td>
							<td>
								<div style="text-align: right;">
									<a class="delete-exam crud" id="exam-[[exam_id]]" href="[[exam_id]]">
										<img class="crud" src="<?=SMP_IMAGES?>/delete.png">
									</a>
									<a class="update-exam crud" id="exam-[[exam_id]]" href="[[exam_id]]">
										<img class="crud" src="<?=SMP_IMAGES?>/update.png">
									</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<?php
			foreach($exams_for_students as $student_id => $exams){
				?>
				<div class="exams-for-student" id="exams-for-student-<?=$student_id?>">
					<h2>Exams for student: <?=$exams[0]->student_name?></h2>
					<table class="grid ui-widget ui-widget-content">
						<thead>
						<tr>
							<th>Name</th>
							<th>Date</th>
							<th>Mark</th>
							<th></th>
						</tr>
						</thead>
						<tbody class="exams">
						<?php
						foreach($exams as $exam){
						?>
							<tr>
								<td><?=$exam->exam_name?></td>
								<td><?=$exam->date_of_exam?></td>
								<td><?=$exam->mark?></td>
								<td>
									<div style="text-align: right;">
										<a class="delete-exam crud" id="exam-<?=$exam->exam_id?>" href="<?=$exam->exam_id?>">
											<img class="crud" src="<?=SMP_IMAGES?>/delete.png">
										</a>
										<a class="update-exam crud" id="exam-<?=$exam->exam_id?>" href="<?=$exam->exam_id?>">
											<img class="crud" src="<?=SMP_IMAGES?>/update.png">
										</a>
									</div>
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</div>
				<?php
			}
			?>
		</div>
		<?php if (is_admin()) printf('<input type="hidden" name="token" id="token" value="%s"/>', $token); ?>
		<br class="clear">
	</div>
</div>